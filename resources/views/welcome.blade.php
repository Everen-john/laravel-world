<html lang="{{ str_replace('_','-', app()-> getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">

    <title>Laravel</title>
</head>

<body>
    <div id="root">

        <script src="{{ asset('js/app.js')}}"></script>
    </div>


</body>

</html>
