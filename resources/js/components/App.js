import { Carousel } from "bootstrap";
import React from "react";
import ReactDOM from "react-dom";

function App() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className=" col-lg-6">
                    <div className="card">
                        <div className="card-header">Example Component</div>

                        <div className="card-body">I edited a component!</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;

if (document.getElementById("root")) {
    ReactDOM.render(<App />, document.getElementById("root"));
}
